<!DOCTYPE html>
<html>

    <head>
        <title>Log in</title>
        <link rel="stylesheet" href="prijava-style.css"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="js/actions.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/b889979efb.js" crossorigin="anonymous"></script>

    </head>

    <body>
        <div>
            <?php 
            include "db.php";
            
            session_start();
            
            #Login script is begin here
            #If user given credential matches successfully with the data available in database then we will echo string login_success
            #login_success string will go back to called Anonymous funtion $("#login").click() 
            
            if(isset($_POST["email"]) && isset($_POST["password"])){
                $email = mysqli_real_escape_string($con, $_POST["email"]);
                $password = $_POST["password"];
            
                // Retrieve the hashed password from the database based on the email
                $sql = "SELECT * FROM user_info WHERE email = '$email'";
                $run_query = mysqli_query($con, $sql);
                $count = mysqli_num_rows($run_query);
                $row = mysqli_fetch_assoc($run_query);
            
                if ($row) {
                    $storedHashedPassword = $row["password"];
            
                    // Verify the entered password against the stored hashed password
                    if (password_verify($password, $storedHashedPassword)) {
                        // Password is correct
                        $_SESSION["uid"] = $row["user_id"];
                        $_SESSION["name"] = $row["first_name"];
                        $ip_add = getenv("REMOTE_ADDR");
                    //we have created a cookie in login_form.php page so if that cookie is available means user is not login
                    
                        //if user record is available in database then $count will be equal to 1
                        if($count == 1){
                                
                            if (isset($_COOKIE["product_list"])) {
                                $p_list = stripcslashes($_COOKIE["product_list"]);
                                //here we are decoding stored json product list cookie to normal array
                                $product_list = json_decode($p_list,true);
                                for ($i=0; $i < count($product_list); $i++) { 
                                    //After getting user id from database here we are checking user cart item if there is already product is listed or not
                                    $verify_cart = "SELECT id FROM cart WHERE user_id = $_SESSION[uid] AND p_id = ".$product_list[$i];
                                    $result  = mysqli_query($con,$verify_cart);
                                    if(mysqli_num_rows($result) < 1){
                                        //if user is adding first time product into cart we will update user_id into database table with valid id
                                        $update_cart = "UPDATE cart SET user_id = '$_SESSION[uid]' WHERE ip_add = '$ip_add' AND user_id = -1";
                                        mysqli_query($con,$update_cart);
                                    }else{
                                        //if already that product is available into database table we will delete that record
                                        $delete_existing_product = "DELETE FROM cart WHERE user_id = -1 AND ip_add = '$ip_add' AND p_id = ".$product_list[$i];
                                        mysqli_query($con,$delete_existing_product);
                                    }
                                }
                                //here we are destroying user cookie
                                setcookie("product_list","",strtotime("-1 day"),"/");
                                //if user is logging from after cart page we will send cart_login
                                echo "cart_login";
                                
                                
                                exit();
                                
                            }
                            //if user is login from page we will send login_success
                            echo "login_success";
                            $BackToMyPage = $_SERVER['HTTP_REFERER'];
                                if(!isset($BackToMyPage)) {
                                    header('Location: '.$BackToMyPage);
                                    echo"<script type='text/javascript'>
                                    
                                    </script>";
                                } else {
                                    header('Location: index.php'); // default page
                                }
                            exit;
                        } else {
                            $email = mysqli_real_escape_string($con,$_POST["email"]);
                            $password =md5($_POST["password"]) ;
                            $sql = "SELECT * FROM admin_info WHERE admin_email = '$email' AND admin_password = '$password'";
                            $run_query = mysqli_query($con,$sql);
                            $count = mysqli_num_rows($run_query);
            
                            //if user record is available in database then $count will be equal to 1
                            if($count == 1){
                                $row = mysqli_fetch_array($run_query);
                                $_SESSION["uid"] = $row["admin_id"];
                                $_SESSION["name"] = $row["admin_name"];
                                $ip_add = getenv("REMOTE_ADDR");
                                //we have created a cookie in login_form.php page so if that cookie is available means user is not login
                
                                //if user is login from page we will send login_success
                                echo "login_success";
                                exit;
                
                            } else {
                                echo "<span style='color:red;'>Please register before login..!</span>";
                                exit();
                            }
                        }
                    }
                }    
            }
            ?>
        </div>

        <form action="" method="post">
            <div class="container">
                <h1>Login</h1>
                <hr>

                <label for="email"><b>Email</b></label>
                <input type="text" name="email" placeholder="Enter your email" id="password" required>

                <label for="password"><b>Lozinka</b></label>
                <input type="password" name="password" placeholder="Enter your password" id="password" required>

                <div class="clearfix">
                    <button class="cancelbtn" type="button" onclick="location.href='index.php'">Cancel</button>
                    <button class="signupbtn" type="submit" name= "login">Login</button>
                </div>

                <button type="button" onclick="location.href='registracija.php'">Want to register?</button><hr>


                <button type="button" class="forgot-password" data-bs-toggle="modal" data-bs-target="#forgotPasswordModal">Forgot Password?</button>
            </div>
        </form>

        <!-- Password Recovery Modal -->
        <div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="forgotPasswordModalLabel">Forgot Password</h5>
                    </div>
                    <div class="modal-body">
                        <input type="text" id="recoveryEmail" class="form-control" placeholder="Enter your email" required>
                        <input type="text" id="recoveryMobile" class="form-control" placeholder="Enter your phone number" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="recoverPasswordButton">Recover Password</button>
                    </div>
                    <div id="recoveryResponse"></div>
                </div>
            </div>
        </div>
    </body>
</html>

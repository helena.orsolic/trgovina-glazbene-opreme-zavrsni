<!DOCTYPE html>
<html>

<head>
    <title>Password Reset</title>
    <link rel="stylesheet" href="prijava-style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="js/actions.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://kit.fontawesome.com/b889979efb.js" crossorigin="anonymous"></script>
</head>

<style>
    .container {
        max-width: 600px;
        margin: 0 auto;
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 5px;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    }

    .form-group {
        margin-bottom: 20px;
    }

    .form-group label {
        display: block;
        font-weight: bold;
        margin-bottom: 5px;
    }

    .form-control {
        width: 100%;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    .btn-primary {
        width: 100%;
    }

    .error-message {
        color: red;
        background-color: #f8d7da;
        border: 1px solid #f5c6cb;
        padding: 10px;
        margin-bottom: 10px;
        border-radius: 3px;
    }
</style>

<body>
    <div class="container">
        <?php
        include "db.php";
        // Get the email from the URL parameter
        $email = $_GET["email"];

        if (isset($_POST["resetPassword"])) {
            $newPassword = $_POST["newPassword"];
            // Update the password for the user with the given email
            $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);
            $updateQuery = "UPDATE user_info SET password = '$hashedPassword' WHERE email = '$email'";
            $updateResult = mysqli_query($con, $updateQuery);

            if ($updateResult) {
                echo '<div class="alert alert-success" role="alert">Password updated successfully</div>
                <button class="btn btn-success mt-3"><a href="prijava.php" class="text-white">Go to Login page</a></button>
                ';

            } else {
                echo '<div class="alert alert-danger" role="alert">Password update failed</div>';
            }
        }
        ?>
        <!-- Your HTML form for resetting the password -->
        <h2>Password Reset</h2>
        <form action="" method="post">
            <div class="form-group">
                <input type="password" id="newPassword" name="newPassword" class="form-control" placeholder="Enter new password" required>
            </div>
            <button type="submit" name="resetPassword" class="btn btn-primary">Reset Password</button>
        </form>
    </div>
</body>

</html>

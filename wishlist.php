<?php
include "db.php";

include "header.php";                    
?>

<link href="css/myorders.css" rel="stylesheet"/>	

<style>

    .wishlist-products .product-details .add-to-cart-btn {
        position: relative;
        border: 2px solid #337ab7;
        height: 40px;
        background-color: #337ab7;
        color: #FFF;
        text-transform: uppercase;
        font-weight: 700;
        border-radius: 40px;
        -webkit-transition: 0.2s all;
        transition: 0.2s all;
        font-size: 12px; /* Adjust the font size as needed */
        padding: 0 10px; /* Adjust the padding as needed */
    }
    .wishlist-products .product-details .add-to-wish-btn {
        position: relative;
        border: 2px solid red;
        height: 40px;
        background-color: red;
        color: #FFF;
        text-transform: uppercase;
        font-weight: 700;
        border-radius: 40px;
        -webkit-transition: 0.2s all;
        transition: 0.2s all;
        font-size: 12px; /* Adjust the font size as needed */
        padding: 0 10px; /* Adjust the padding as needed */
    }

</style>

			
<section class="section main main-raised">       
	<div class="container-fluid ">
		<div class="wrap cf">
            <h1 class="projTitle">Your Wishlist</h1>
            <div class="cart">
                <ul class="cartWrap">
                <!-- Display Wishlist Products -->
                <div class="wishlist-products">
                    <div class="row">
                        <?php
                        if (isset($_SESSION['uid'])) {
                            $user_id = $_SESSION['uid'];

                            $wishlistQuery = "SELECT wishlist.*, products.product_title, products.product_image, products.product_price
                                            FROM wishlist
                                            INNER JOIN products ON wishlist.product_id = products.product_id
                                            WHERE wishlist.user_id = '$user_id'";
                            $wishlistResult = mysqli_query($con, $wishlistQuery);

                            if (mysqli_num_rows($wishlistResult) > 0) {
                                while ($wishlistRow = mysqli_fetch_assoc($wishlistResult)) {
                                    echo '
                                    <li class="items even">
                                        <div class="infoWrap"> 
                                            <div class="cartSection col-md-6">
                                                <a href="product.php?p=' . $wishlistRow['product_id'] . '">
                                                    <img src="product_images/' . $wishlistRow['product_image'] . '" alt="' . $wishlistRow['product_title'] . '" class="itemImg" />
                                                </a>
                                                <h3 style="margin-top: 25px"><a href="product.php?p=' . $wishlistRow['product_id'] . '">' . $wishlistRow['product_title'] . '</a></h3>
                                            </div>  
                                                
                                            <div class="prodTotal cartSection col-md-2" style="margin-top: 30px">
                                                <p>$ ' . $wishlistRow['product_price'] . '</p>
                                            </div>
                                            
                                            <div class="product-details">
                                                <div class="btn-group" style="margin-top: 25px">
                                                    <button class="add-to-cart-btn reload" id="product" pid="' . $wishlistRow['product_id'] . '">
                                                        <i class="fa fa-shopping-cart"></i>Add to Cart
                                                    </button>
                                                    <a href="wishlist.php">
                                                        <button class="add-to-wish-btn wishlist-remove" remove_id="' . $wishlistRow['product_id'] . '">
                                                            <i class="fa fa-trash-o"></i>
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    ';
                                }
                            } else {
                                echo '<p class="empty-wishlist">Your wishlist is empty.</p>';
                            }
                        } else {
                            echo '<p class="login-message">Please log in to view your wishlist.</p>';
                        }
                        ?>
                    </div>
                </div>
                <!-- /Display Wishlist Products -->
 
                <div class="heading cf">
                    <a href="store.php" class="continue">Continue Shopping</a>
                </div>
                </ul>
            </div> 
        </div>
    </div>
 </section>


<?php
include "footer.php";
?>
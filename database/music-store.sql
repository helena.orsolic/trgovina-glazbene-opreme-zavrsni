-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2023 at 09:04 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `music-store`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getcat` (IN `cid` INT)   SELECT * FROM categories WHERE cat_id=cid$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `admin_id` int(10) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(300) NOT NULL,
  `admin_password` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'admin', 'admin@gmail.com', 'malalampa');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(100) NOT NULL,
  `brand_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_title`) VALUES
(1, 'Red Hill'),
(2, 'Yamaha'),
(3, 'Dunlop'),
(4, 'Epiphone'),
(5, 'Gibson'),
(6, 'J & D'),
(7, 'Pearl'),
(8, 'Fame'),
(9, 'D\'Addario'),
(10, 'Rockson'),
(11, 'Sony'),
(12, 'Mackie');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) NOT NULL,
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(250) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `p_id`, `ip_add`, `user_id`, `qty`) VALUES
(156, 11, '::1', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(100) NOT NULL,
  `cat_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(1, 'Acoustic Guitar'),
(2, 'Electric Guitar\r\n'),
(3, 'Bass Guitar\r\n'),
(4, 'Drums'),
(5, 'Accessories');

-- --------------------------------------------------------

--
-- Table structure for table `email_info`
--

CREATE TABLE `email_info` (
  `email_id` int(100) NOT NULL,
  `email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `email_info`
--

INSERT INTO `email_info` (`email_id`, `email`) VALUES
(3, 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `trx_id` varchar(255) NOT NULL,
  `p_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `product_id`, `qty`, `trx_id`, `p_status`) VALUES
(1, 12, 7, 1, '07M47684BS5725041', 'Completed'),
(2, 14, 2, 1, '07M47684BS5725041', 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `orders_info`
--

CREATE TABLE `orders_info` (
  `order_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `cardname` varchar(255) NOT NULL,
  `cardnumber` varchar(20) NOT NULL,
  `expdate` varchar(255) NOT NULL,
  `prod_count` int(15) DEFAULT NULL,
  `total_amt` int(15) DEFAULT NULL,
  `cvv` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `orders_info`
--

INSERT INTO `orders_info` (`order_id`, `user_id`, `f_name`, `email`, `address`, `city`, `state`, `zip`, `cardname`, `cardnumber`, `expdate`, `prod_count`, `total_amt`, `cvv`) VALUES
(2, 16, 'venky vs', 'venkey@gmail.com', 'neka adresa', 'Podgorica', 'Nema', 674568, 'ime prezime', '4875 2469 7468 5353', '12/26', 1, 1154, 889),
(3, 26, 'idjfgb fdgbyfby', 'empty@trans.hr', 'josipa', 'bfdusbfcsjv', 'Nema', 456756, 'ime prezime', '6549 8798 7879 8798', '12/12', 1, 398, 222),
(4, 3, 'Rozalija Zivkovic', 'rozalija@gmail.com', 'Osijek', 'Moja Adresa', 'Nema', 822228, 'ime prezime', '4564 5654 6546 4566', '12/23', 1, 66, 213),
(5, 2, 'Helena Orsolic', 'empty@trans.hr', 'Josipa Obrenovica', 'Krapina', 'Nema', 123456, 'ime prezime', '1111111111111111', '12/26', 1, 1154, 885),
(6, 4, 'Petra Popovic', 'petra@gmail.com', 'Zagreb', 'ZagrebAdres', 'Nema', 123456, 'ime prezime', '123456789456', '12/26', 1, 66, 822),
(7, 4, 'Petra Popovic', 'petra@gmail.com', 'Zagreb', 'ZagrebAdres', 'Nema', 123456, 'ime prezime', '123456789789', '12/23', 1, 79, 888),
(8, 4, 'Petra Popovic', 'petra@gmail.com', 'Zagreb', 'ZagrebAdres', 'Nema', 123456, 'ime prezime', '123456789', '12/23', 1, 1154, 888),
(9, 4, 'Petra Popovic', 'petra@gmail.com', 'Zagreb', 'ZagrebAdres', 'Nema', 123558, 'ime prezime', '12316546578764', '12/23', 2, 72, 222);

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `order_pro_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(15) DEFAULT NULL,
  `amt` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`order_pro_id`, `order_id`, `product_id`, `qty`, `amt`) VALUES
(10, 2, 11, 1, 1154),
(11, 3, 7, 1, 398),
(12, 4, 1, 1, 66),
(13, 5, 11, 1, 1154),
(14, 6, 1, 1, 66),
(15, 7, 6, 1, 79),
(16, 8, 11, 1, 1154),
(17, 9, 1, 1, 66),
(18, 9, 14, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(100) NOT NULL,
  `product_cat` int(100) NOT NULL,
  `product_brand` int(100) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_keywords` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_cat`, `product_brand`, `product_title`, `product_price`, `product_desc`, `product_image`, `product_keywords`) VALUES
(1, 1, 1, 'Red Hill D-1 SB Sunburst', 66, 'Laminated dreadnought acoustic guitar with basswood top, basswood body, mahogany neck and composite fretboard\r\nThe Red Hill D-1 SB is aimed at beginners and advanced players who are looking for a reliable acoustic guitar at a very good price-performance ratio.', 'red-hill-d-1-sb-sunburst_1_GIT0047803-000.jpg', 'red hill acoustic'),
(2, 1, 1, 'Red Hill 000-5-S Auditorium', 99, 'Auditorium Acoustic Guitar (Natural)\r\nThe Red Hill OM Mahogany is a 6-String Auditorium Acoustic Guitar and an excellent choice for learning the Instrument or purchasing a second Guitar. The OM Mahogany uses a mixture of tone-woods; a Sandalwood Back and Sides compliments a Sapele Top; the 20-Fret ‘C’ Neck has been crafted from Mahogany; and the Fingerboard from a Composite.\r\n\r\nA compensated Bone Bridge inlay provides optimal resonance transmission and a flat string action, while the die-cast machine heads keep the tuning clean. The flat ‘C’ Neck profile lies pleasantly in the hand and ensures a soft and comfortable playability. The discreet but chic look is completed by a natural binding and a Rosette of Abalone and Dots on the Fingerboard.', 'red-hill-d-1-nt-natural_1_GIT0056666-000.jpg', 'red hill acoustic'),
(3, 1, 6, 'J & D AG-1 NT Natural', 65, 'Beginner acoustic guitar with top out Agathis and body out Linde\r\nThe J & D AG-1 Natural Acoustic Guitar is a six-string acoustic guitar with an Agathis top, Basswood back and sides and a Rosewood fingerboard and bridge. This guitar is light and airy and has sweet tonal qualities brought out by the light Agathis wood top.\r\n\r\nThe scale length is 648mm while the nut width is 43mm.\r\n\r\nThis guitar is finished in a natural gloss that brings out the lighter tones of the agathis.\r\n\r\nThis is perfect for the beginner or the student.', 'j-und-d-ag-1-nt-natural_1_GIT0030772-000.jpg', 'j & d acoustic'),
(4, 2, 6, 'J & D ST-MINI Sunburst', 88, 'ST-Style Mini Electric Guitar with 22,2\" scale length\r\nThe J&D ST-Mini Electric Guitar, Sunburst is ideally suited to children who want to start playing guitar from a very young age but require a smaller, lightweight guitar that they can play with ease.\r\n\r\nAlthough aimed at children, the ST-Mini has been constructed using professional grade components including an alder body, bolted on maple neck and a 22 fret rosewood fingerboard layered over a 564mm scale making it perfect for smaller hands.\r\n\r\nThe J&D ST-Mini also features one Humbucker and one Single Coil pickup that generate an array of classic sounds. The guitar also has die cast machine heads, a vintage tremolo, has been built using chrome hardware and is finished in a beautiful shade of sunburst.', 'j-und-d-st-mini-sunburst_1_GIT0007454-000.jpg', 'j & d electric'),
(5, 2, 10, 'Rockson ST White', 65, 'ST-style electric guitar with three ceramic single coils\r\nWith the Rockson ST Electric Guitar White electric guitar, the manufacturer offers one of the most famous guitar models at an absolute budget price. The guitar has a Poplar body in the famous ST shape and a bolt-on maple neck, with a slim \'C\' neck profile that ensures smooth playability in all positions of the 22-fret composite fretboard. The electronics also rely on traditional specifications and are made up of three single-coil pickups fitted with ceramic magnets and the classic knob layout with master volume control and two separate tone controls. Last but not least, the Rockson ST Electric Guitar White offers a chic as well as functional hardware equipment in a uniform chrome finish.', 'rockson-st-electric-guitar-white_1_GIT0050070-000.jpg', 'rockson electric'),
(6, 2, 10, 'Rockson TL Natural White ', 79, 'The Rockson TL Special Natural combines the iconic TL-style guitar design with the comfortable benefits of its weight-saving construction thanks to the use of Paulownia wood. The wood of the bluebell tree is characterized not only by its feather-light weight but also by good vibration characteristics, giving the Rockson TL Special its own characterful sound. Here are played on the amplifier additionally responsible for two ceramic single coils, which know how to produce classic TL sounds thanks to classic wiring with three switching positions, Master Volume and Master Tone. As attachments, on the other hand, a T-style top-load hardtail bridge with three string riders and six reliable die-cast tuning machines are used.', 'rockson-tl-special-natural-white-pickguard_1_GIT0059409-000.jpg', 'rockson electric'),
(7, 3, 4, 'Epiphone EB-3 Bass Cherry', 398, 'The Epiphone EB-3 4-String Bass Guitar, Mahogany Body and Neck, Cherry Finish. Small, Classic \'60s bass with ultra-thin body, lightning fast neck and distinctive double cutaway shape.\r\n\r\nThe Epiphone EB-3 bass guitar is a true reproduction of the Gibson SG bass, one of rock\'s defining instruments. Authorized by Gibson, Epiphone has brought back a classic bass guitar of the early \'60s. The Epiphone EB-3 bass has a mahogany body and a set mahogany neck to give it the sustain that made the original famous. A Sidewinder humbucker and mini humbucker deliver the tonal power. The neck profile is fast and very comfortable while the chrome hardware, rosewood fingerboard and trapezoid inlays make this bass a comfort to play and watch. The Epiphone EB-3 bass provides the tone and the look that inspires musicians.', 'epiphone-eb-3-bass-cherry_1_BAS0002415-001.jpg', 'epiphone bass'),
(8, 3, 4, 'Epiphone EB-3 Bass Ebony', 408, 'The Epiphone EB-3 SG STYLE 4-String Bass Guitar, Mahogany Body and Neck, Cherry Finish. Small, Classic \'60s SG style bass with ultra-thin body, lightning fast neck and distinctive double cutaway shape.\r\n\r\nThe Epiphone EB-3 SG Style bass guitar is a true reproduction of the Gisbon SG bass, one of rock\'s defining instruments. Authorized by Gibson, Epiphone has brought back a classic bass guitar of the early \'60s. The Epiphone EB-3 bass has a mahogany body and a set mahogany neck to give it the sustain that made the original famous. A Sidewinder humbucker and mini humbucker deliver the tonal power. The neck profile is fast and very comfortable while the chrome hardware, rosewood fingerboard and trapezoid inlays make this bass a comfort to play and watch. The Epiphone EB-3 bass provides the tone and the look that inspires musicians.', 'epiphone-eb-3-bass-ebony_1_BAS0002679-000.jpg', 'epiphone bass'),
(9, 3, 4, 'Epiphone Thunderbird 60s Bass', 681, '4-string bass with ProBucker bass #760 pickups\r\nWith the Epiphone Thunderbird 60s Bass Tobacco Sunburst Bass, Epiphone presents an accurate reproduction of the legendary 1963 Thunderbird, which is considered one of the most classic rock basses, as part of the Inspired by Gibson Collection. Based on the proven neck-thru construction, this four-string bass guitar features a seven-piece mahogany-walnut neck and mahogany body wings, which together form the basis for its extremely powerful sound. The \"1960s Rounded\" neck profile offers excellent playing comfort. Two Epiphone ProBucker Bass #760 humbuckers provide extremely powerful bass sounds with a massive low end foundation and a fat midrange spectrum when plugged into an amp, letting you feel every rock or metal groove with your whole body. Open-gear tuners, a Tune-O-Matic bridge and the classic Claw tailpiece complete the Epiphone Thunderbird 60s Bass Tobacco Sunburst electric bass.\r\n\r\n', 'epiphone-thunderbird-60s-bass-tobacco-sunburst_1_BAS0010731-000.jpg', 'epiphone bass'),
(10, 4, 8, 'Fame First Step Studio Drum', 366, 'Drums Complete Set for Beginners - Fame First Step Drums\r\nThe Fame FS20B First Step Studio Set Piano Black is a solid complete set for drum beginners with cymbals, hardware and throne. The high-quality crafted mixed wood drum shells deliver clean and balanced sound with plenty of sustain and sufficient bass. A sturdy hardware package consisting of cymbal stand, hi hat stand, drum throne and bass drum pedal ensures firm footing and individual adjustment options. The heads rest neatly on the edges and are easy to tune. The bass drum head is pre-damped, which allows excellent control of the sound. Practice cymbals in the form of a 14\" hi hat and an 18\" crash ride cymbal promise excellent feel and classic cymbal sound. One of the highlights is the snare drum made of wood, which provides a dry and warm sound. A unique feature is also the matte aluminum: In addition to the modern, high-quality look, this results in a reduced transport weight. The modern aluminum element is also used for the stands, which creates a harmonious overall picture.', 'fame-fs20b-first-step-studio-drum-set-piano-black-_1_DRU0039823-000.jpg', 'fame drums'),
(11, 4, 7, 'Pearl Export Black Cherry Glitter', 1154, 'Complete drumset from the legendary Pearl Export series\r\nThe drumsets of the Pearl Export series come as complete kits incl. hardware and cymbals. They are perfect for beginners and advanced drummers. In the EXX725BR/C configuration, the Export set comes with a 22\" bass drum, 12\" and 13\" toms as well as a 16\" floor tom and a 14\" snare. The high quality poplar shells with Opti-Loc tom mounting system and high-end hardware make this set a great choice. When it comes to looks, this set impresses with the newly introduced color Black Cherry Glitter. A professional look for a professional set.', 'pearl-export-exx725br-c-black-cherry-glitter-704_1_DRU0035977-000.jpg', 'pearl drums'),
(12, 4, 7, 'Pearl Roadshow Studio Bronze ', 579, 'Complete beginner drum set from Pearl with Sabian cymbals.\r\nThe Pearl RS505C/C707 Roadshow Studio Bronze Metallic is a fully equipped drum set for beginners. It impresses with a Sabian cymbal set, high-quality poplar cymbals and a sturdy hardware package with stool. The shells are made of six layers of poplar with a thickness of 6 mm. Thanks to excellent manufacturing quality of the Pearl Roadshow series, the acoustic drum set offers balanced sound with high bass content and excellent response. The accurate shell burrs of 45 ° result in combination with the newly developed dual lugs an ideal skin support and easy tuning.', 'pearl-rs505c-c707-roadshow-studio-bronze-metallic_1_DRU0040891-000.jpg', 'pearl drums'),
(13, 5, 9, 'D\'Addario PW-CT 26 Nexxus Tuner', 26, 'The Planet Waves PW-CT-26 Nexxus 360 Tun er is a convenient music accessory with numerous benefits. The chromatic contact tuner allows for quick and precise tuning while being easy to use.\r\n\r\nIt is a rechargeable tuner that puts an end to changing and searching for full batteries. This is done with the included USB cable, which can be connected to any USB device. This provides 24 hours of runtime per charge. The tuner is easily attached to the head of the musical instrument by means of the clamp, where it can easily remain during playing for constant control without interfering. The direct contact makes it particularly suitable for musicians who often have to tune their instrument in noisy environments. By means of a vibration sensor, it measures the pitch on the basis of the vibrations. This can be read from the bright LED-Display, which is easy to see even in suboptimal lighting conditions. For maximum flexibility, the tuner can be rotated 360°. This gives the best possible operation and readability from any position. The controls are located on the front of the unit and are clearly and unambiguously marked.', 'd-addario-pw-ct-26-nexxus-360-tuner_1_ACC0010425-000.jpg', 'tuner'),
(14, 5, 3, 'Dunlop Tortex Guitar Picks 0.73', 6, 'Pack of 12 yellow Jim Dunlop Tortex picks\r\n\r\nThe Jim Dunlop 418P Tortex Standard picks are used by top musicians worldwide. Tortex picks are carefully designed and manufactured to give the characteristic maximum memory and minimum wear that made original tortoise shell famous.', 'dunlop-tortex-guitar-picks-0-73-pack-of-12_1_ACC0002015-000.jpg', 'picks dunlop'),
(15, 5, 8, 'Fame MSC-02 Flat Capo 6-String', 7, 'The Fame MSC-02 Flat Capo (6-String Guitar) is a sturdy Zinc Capo designed for use with Acoustic and Electric 6-String Guitars that include flat fingerboards. The MSC-02 employs a clamping lever to safely and easily attach to your instrument.\r\n\r\nMusicians use Capos to raise the pitch of their instruments by attaching them to the neck below the nut, therefore effectively bringing the nut closer to the body.', 'fame-msc-02-flat-capo-6-string-guitar-_1_GIT0033786-000.jpg', 'fame capo'),
(16, 5, 5, 'Gibson Plectrum Standard Thin Picks', 1, '2012-Winter-Sweater-for-Men-for-better-outlook', 'gibson-plectrum-standard-thin_1_ACC0002104-000.jpg', 'gibson pick'),
(17, 5, 12, 'Mackie Big Knob Studio Monitor Controller', 230, 'Mackie Big Knob Studio is a monitor controller that can switch three stereo sources to two pairs of monitors. On the other hand, it can also be used as an audio interface, featuring top-notch Onyx™ PreAmps and 96-kHz/24-bit converters. Thanks to its sophisticated features like the built-in talkback microphone and the extra input for smartphones, the Big Knob Studio is a compact solution for any studio.', 'mackie-big-knob-studio-_5_REC0012959-000.jpg', 'monitor controller'),
(18, 5, 11, 'Sony MDR-7506 Monitor Headphones\r\n', 104, 'The Sony MDR-7506 Professional Monitor Headphones designed for professional studio and live/broadcast applications. The MDR7506 can be used for recording and playback.\r\n\r\nThe MDR7506 Headphones have a closed-ear design for comfort and reduction of external noise interference. Also a folding construction for portability and easy storage.', 'sony-mdr-7506-professional-monitor-headphones-_1_REC0000388-000.jpg', 'sony headphones'),
(19, 5, 2, 'Yamaha THR 5A Guitar Amplifier', 209, 'he Yamaha THR5A Acoustic Guitar Amp is an acoustic guitar amp and recording interface. The THR5A is optimized for use with electric-acoustic and silent guitars. Utilising advanced modelling technologies developed by Yamaha, THR5A offers simulations of classic tube condenser and dynamic mics combined with studio-grade effects to create recording-studio tone direct from your guitar and wherever you are.', 'yamaha-thr-5a_1_GIT0025890-000.jpg', 'amp yamaha'),
(20, 5, 2, 'Yamaha THR 30 Guitar Amplifier', 469, 'The Yamaha THR 30 IIA Acoustic Guitar Amplifier offers a wide range of effect combinations and modulations, as well as a connection for vocal microphones and a headphone jack. In cooperation with Line6, it is also possible to connect wirelessly with the optionally available Line6 Relay G10T transmitter, so that you can enjoy unlimited freedom of movement while playing. An integrated rechargeable battery also allows for untethered use outside of the rehearsal room or your own four walls. In addition, an aux-in jack and Bluetooth function allow playback from external sources. With its lightweight construction of just 6.4 kg and its small dimensions, the compact sound miracle is extremely comfortable to transport.', 'yamaha-thr-30-iia-acoustic-guitar-amplifier_1_GIT0054902-000.jpg', 'yamaha amp');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address1`, `address2`) VALUES
(1, 'venky', 'vs', 'venkey@gmail.com', '1234534', '9877654334', 'snhdgvajfehyfygv', 'asdjbhfkeur'),
(2, 'Helena', 'Orsolic', 'empty@trans.hr', '123123123', '4568765568', 'Josipa Obrenovica', 'Krapina'),
(3, 'Rozalija', 'Zivkovic', 'rozalija@gmail.com', 'nekasifra', '9564589786', 'Osijek', 'Moja Adresa'),
(4, 'Petra', 'Popovic', 'petra@gmail.com', '123456789', '4567899598', 'Zagreb', 'ZagrebAdres'),
(5, 'Helena', 'Orsolic', 'neka@gmail.com', '555555555', '4568765568', 'adresa', 'grad'),
(6, 'Karlo', 'Marlo', 'kmarlo@gmail.com', 'sifrasifra', '1234567891', 'adresa', 'grad'),
(8, 'imeime', 'prezimeprezime', 'neki.email@gmail.com', '$2y$10$rkm4ADo17CSZ1bA2WKKNwe3mCeNtb91hn1PathPtHmvcMpIgV2BKe', '1234567899', 'adresa', 'grad');

--
-- Triggers `user_info`
--
DELIMITER $$
CREATE TRIGGER `after_user_info_insert` AFTER INSERT ON `user_info` FOR EACH ROW BEGIN 
INSERT INTO user_info_backup VALUES(new.user_id,new.first_name,new.last_name,new.email,new.password,new.mobile,new.address1,new.address2);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_info_backup`
--

CREATE TABLE `user_info_backup` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `user_info_backup`
--

INSERT INTO `user_info_backup` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address1`, `address2`) VALUES
(1, 'venky', 'vs', 'venkey@gmail.com', '1234534', '9877654334', 'snhdgvajfehyfygv', 'asdjbhfkeur'),
(2, 'Helena', 'Orsolic', 'empty@trans.hr', '123123123', '4568765568', 'Josipa Obrenovica', 'Krapina'),
(4, 'Petra', 'Popovic', 'petra@gmail.com', '123456789', '4567899598', 'Zagreb', 'ZagrebAdres'),
(5, 'Helena', 'Orsolic', 'neka@gmail.com', '555555555', '4568765568', 'adresa', 'grad'),
(6, 'Karlo', 'Marlo', 'kmarlo@gmail.com', 'sifrasifra', '1234567891', 'adresa', 'grad'),
(7, 'rtsyhfgshy', 'fhyghdfgy', 'f<g<dg<fdgdy', 'ycvghxdzuhdth', 'ghxgfhxfth', 'gfhxtg', 'gfhxgxrth'),
(8, 'imeime', 'prezimeprezime', 'neki.email@gmail.com', '$2y$10$rkm4ADo17CSZ1bA2WKKNwe3mCeNtb91hn1PathPtHmvcMpIgV2BKe', '1234567899', 'adresa', 'grad'),
(27, 'Petra', 'Popovic', 'petra@gmail.com', 'nekasifra', '9564589786', 'Osijek', 'Josipa Hutt'),
(28, 'Petra', 'Popovic', 'petra@gmail.com', 'nekasifra', '9564589786', 'Osijek', 'Josipa Hutt'),
(29, 'idjfgb', 'fdgbyfby', 'empty@trans.com', '456789456789', '9564589786', 'bfdusbfcsjv', 'Hrvatska'),
(30, 'ide gas', 'nema gasa', 'rozalija.zivkovic@gmail.com', '123456789', '9564589786', 'asdsdas', 'Hrvatska'),
(31, 'Rozalija', 'Zivkovic', 'rozalija@gmail.com', 'nekasifra', '9564589786', 'Osijek', 'Moja Adresa');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `email_info`
--
ALTER TABLE `email_info`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `orders_info`
--
ALTER TABLE `orders_info`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`order_pro_id`),
  ADD KEY `order_products` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_info_backup`
--
ALTER TABLE `user_info_backup`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `admin_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `email_info`
--
ALTER TABLE `email_info`
  MODIFY `email_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders_info`
--
ALTER TABLE `orders_info`
  MODIFY `order_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `order_pro_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_info_backup`
--
ALTER TABLE `user_info_backup`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products` FOREIGN KEY (`order_id`) REFERENCES `orders_info` (`order_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

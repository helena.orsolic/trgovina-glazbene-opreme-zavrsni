<!DOCTYPE html>
<html>

    <head>
        <title>Admin login</title>
        <link rel="stylesheet" href="prijava-style.css"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/b889979efb.js" crossorigin="anonymous"></script>
    </head>

    <body>
        <div>
            <?php 
            include "db.php";
            $msg='';
            if (isset($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])){
                $sql="SELECT * FROM admin_info";
                $q=mysqli_query($con,$sql);
            

                while ($redak = mysqli_fetch_array($q,MYSQLI_ASSOC)){
                    if(($_POST)['email'] == $redak["admin_email"] && $_POST['password'] == $redak['admin_password']){
                        header("Location: admin/index.php");
                    }else{
                        $msg = 'Krivo korisničko ime ili lozinka';
                    }
                }
            }
            ?>
        </div>

            <form action="" method="post">
                <div class="container">
                    <h1>Admin login</h1>
                    <hr>

                    <label for="email"><b>Email</b></label>
                    <input type="text" name="email" placeholder="Enter admin email" id="password" required>
    
                    <label for="password"><b>Lozinka</b></label>
                    <input type="password" name="password" placeholder="Enter admin password" id="password" required>
    
                    <div class="clearfix">
                        <button class="cancelbtn" type="button" onclick="location.href='index.php'">Cancel</button>
                        <button class="signupbtn" type="submit" name= "login">Sign in as Admin</button>
                    </div>
                    
                    <h4><?php echo $msg; ?></h4>
                </div>
            </form>
    </body>
</html>

<?php
include "header.php";
?>

<style>
	.fifty-chars {
    width: 150ch;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}

#review-section {
    margin-top: 20px;
}

.review {
    border: 1px solid #ddd;
    padding: 10px;
    margin-bottom: 15px;
    background-color: #f9f9f9;
}

.comment {
    font-size: 16px;
    margin-bottom: 5px;
}

.rating {
    color: #f39c12;
    font-weight: bold;
}

.date {
    color: #95a5a6;
    font-size: 12px;
}

</style>

<!-- /BREADCRUMB -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
		});
	});
</script>

<script>
    (function (global) {
	if(typeof (global) === "undefined")
	{
		throw new Error("window is undefined");
	}
    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };	
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };
    global.onload = function () {        
		noBackPlease();
		document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            e.stopPropagation();
        };		
    };
})(window);
</script>



<!-- SECTION -->
<div class="section main main-raised">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- Product main img -->
			
			<?php 
				include 'db.php';

				if (isset($_GET['p'])) {
					$product_id = $_GET['p'];
				
				
					$sql = " SELECT * FROM products WHERE product_id = $product_id";
					if (!$con) {
						die("Connection failed: " . mysqli_connect_error());
					}

					$result = mysqli_query($con, $sql);

					if (mysqli_num_rows($result) > 0) {
						while($row = mysqli_fetch_assoc($result)) {
						echo '
							<div class="col-md-5 col-md-push-2">
								<div id="product-main-img">
									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'" alt="">
									</div>

									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'" alt="">
									</div>

									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'" alt="">
									</div>

									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'" alt="">
									</div>
								</div>
							</div>
					
							<div class="col-md-2  col-md-pull-5">
								<div id="product-imgs">
									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'" alt="">
									</div>

									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'" alt="">
									</div>

									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'g" alt="">
									</div>

									<div class="product-preview">
										<img src="product_images/'.$row['product_image'].'" alt="">
									</div>
								</div>
							</div>
							';
								
				?>

				<!-- FlexSlider -->		
				<?php
				// Calculate the average rating for the product based on reviews
				$averageRatingQuery = "SELECT AVG(rating) AS average_rating FROM reviews WHERE product_id = $product_id";
				$averageRatingResult = mysqli_query($con, $averageRatingQuery);
				$averageRatingRow = mysqli_fetch_assoc($averageRatingResult);
				$averageRating = $averageRatingRow['average_rating'];
					echo '
						<div class="col-md-5">
							<div class="product-details">
								<h2 class="product-name">'.$row['product_title'].'</h2>
								<div>
									<div class="product-rating">
										Average Rating: ';
										// Display average rating as stars
										for ($i = 1; $i <= 5; $i++) {
											if ($i <= round($averageRating)) {
												echo '<i class="fa fa-star"></i>';
											} else {
												echo '<i class="fa fa-star-o"></i>';
											}
										}
							echo '
									</div>
								</div>

								<div>
									<h3 class="product-price">$'.$row['product_price'].'</h3>
									<span class="product-available">In Stock</span>
								</div>
								';
								$myString = $row['product_desc'];
								if (strlen($myString) > 150) {
									echo substr($myString, 0, 150) . '...';
									} else {
									echo $myString;
									}

								echo '
								<a href="#product-tab">See more</a>

								<div class="add-to-cart">
									<div class="btn-group" style="margin-left: 25px; margin-top: 15px">
										<button class="add-to-cart-btn" pid="'.$row['product_id'].'" id="product" ><i class="fa fa-shopping-cart"></i> add to cart</button>
									</div>	
								</div>

								<div class="btn-group" style="margin-top: 5px; margin-bottom: 15px">
									<ul class="product-btns">
										<li><a href="#" pid="'.$row['product_id'].'" id="wishlist"><i class="fa fa-heart-o"></i> Add to wishlist</a></li>	
									</ul>
								</div>

								<ul class="product-links">
									<li>Share:</li>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-envelope"></i></a></li>
								</ul>
							</div>
						</div>
								
				
						<!-- /Product main img -->

						<!-- Product thumb imgs -->
						
						
						
						<!-- /Product thumb imgs -->

						<!-- Product details -->
						
						<!-- /Product details -->

						<!-- Product tab -->
						<div class="col-md-12">
							<div id="product-tab">
								<!-- product tab nav -->
								<ul class="tab-nav">
									<li class="active"><a data-toggle="tab" href="#tab1">Description</a></li>
									<li><a data-toggle="tab" href="#tab3">Reviews</a></li>
								</ul>
								<!-- /product tab nav -->

								<!-- product tab content -->
								<div class="tab-content">
									<!-- tab1  -->
									<div id="tab1" class="tab-pane fade in active">
										<div class="row">
											<div class="col-md-12">
												<p>'.$row['product_desc'].'</p>
											</div>
										</div>
									</div>
									<!-- /tab1  -->

									<!-- tab3  -->
									<div id="tab3" class="tab-pane fade in">
										<div class="row">

										<!-- Review Form -->
										<div class="col-md-3" id="review-section">
											<h3>Make a comment</h3>
											<div id="review-form" class="review">
												<form class="review-form" id="submit-review-form">
													<textarea class="input" id="comment" name="comment" placeholder="Share Your opinion" required></textarea>
													<div class="input-rating">
														<span>Your Rating: </span>
														<div class="stars">
															<input id="star5" name="rating" value="5" type="radio"><label for="star5"></label>
															<input id="star4" name="rating" value="4" type="radio"><label for="star4"></label>
															<input id="star3" name="rating" value="3" type="radio"><label for="star3"></label>
															<input id="star2" name="rating" value="2" type="radio"><label for="star2"></label>
															<input id="star1" name="rating" value="1" type="radio"><label for="star1"></label>
														</div>
													</div>
													<input type="hidden" id="product_id" name="product_id" value="'.$row['product_id'].'">
													<button class="primary-btn" type="submit" pid="'.$row['product_id'].'">Submit</button>
												</form>
											</div>
										</div>
										<!-- /Review Form -->


										';?>

										<!-- Display Reviews -->
											<div class="col-md-9">
												<div id="review-section">
													<h3>Reviews</h3>
													<?php
													$reviewsQuery = "SELECT * FROM reviews WHERE product_id = $product_id ORDER BY created_at DESC";
													$reviewsResult = mysqli_query($con, $reviewsQuery);
													
													if ($reviewsResult && mysqli_num_rows($reviewsResult) > 0) {
														while ($reviewRow = mysqli_fetch_assoc($reviewsResult)) {
															echo '<div class="review">';
															echo '<p class="comment">' . $reviewRow['comment'] . '</p>';
															echo '<div class="rating">Rating: ' . $reviewRow['rating'] . '/5</div>';
															echo '<div class="date">Posted on: ' . date("F j, Y", strtotime($reviewRow['created_at'])) . '</div>';
															echo '</div>';
														}
													} else {
														echo '<p>No reviews yet.</p>';
													}
													?>
												</div>
											</div>
											<!-- /Display Reviews -->
											<?php echo'

										</div>
									</div>
									<!-- /tab3  -->
								</div>
								<!-- /product tab content  -->
							</div>
						</div>
						<!-- /product tab -->
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /SECTION -->

		<!-- Section -->
		<div class="section main main-raised">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					
					<div class="col-md-12">
						<div class="section-title text-center">
							<h3 class="title">Related Products</h3>
							
						</div>
					</div>
				';
								$_SESSION['product_id'] = $row['product_id'];
								}
							} 

					} else {
						echo 'nije dobro';
					}
			?>	
			<?php
				include 'db.php';
					if (isset($_GET['p'])) {
						$product_id = $_GET['p'];
							
					$product_query = "SELECT * FROM products,categories WHERE product_cat=cat_id AND product_id BETWEEN $product_id AND $product_id+3";
					$run_query = mysqli_query($con,$product_query);
					if(mysqli_num_rows($run_query) > 0){

						while($row = mysqli_fetch_array($run_query)){
							$pro_id    = $row['product_id'];
							$pro_cat   = $row['product_cat'];
							$pro_brand = $row['product_brand'];
							$pro_title = $row['product_title'];
							$pro_price = $row['product_price'];
							$pro_image = $row['product_image'];

							$cat_name = $row["cat_title"];

							echo "
								<div class='col-md-3 col-xs-6'>
								<a href='product.php?p=$pro_id'><div class='product'>
									<div class='product-img'>
										<img src='product_images/$pro_image' style='max-height: 250px;' alt=''>
									</div></a>
									<div class='product-body'>
										<p class='product-category'>$cat_name</p>
										<h3 class='product-name header-cart-item-name'><a href='product.php?p=$pro_id'>$pro_title</a></h3>
										<h4 class='product-price header-cart-item-info'>$pro_price $</h4>
										<div class='product-rating'>
											<i class='fa fa-star'></i>
											<i class='fa fa-star'></i>
											<i class='fa fa-star'></i>
											<i class='fa fa-star'></i>
											<i class='fa fa-star'></i>
										</div>
										<div class='product-btns'>
											<button class='add-to-wishlist'><i class='fa fa-heart-o'></i><span class='tooltipp'>add to wishlist</span></button>
											<button class='add-to-compare'><i class='fa fa-exchange'></i><span class='tooltipp'>add to compare</span></button>
											<button class='quick-view'><i class='fa fa-eye'></i><span class='tooltipp'>quick view</span></button>
										</div>
									</div>
									<div class='add-to-cart'>
										<button pid='$pro_id' id='product' class='add-to-cart-btn block2-btn-towishlist' href='#'>
											<i class='fa fa-shopping-cart'></i> add to cart
										</button>
									</div>
								</div>
								</div>
					
							";
						};
					}
				} else {
					echo 'nije dobro';
				}
			?>
			<!-- product -->
			
			<!-- /product -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /Section -->


<!-- FOOTER -->
<?php
include "footer.php";
?>

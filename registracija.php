<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="prijava-style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://kit.fontawesome.com/b889979efb.js" crossorigin="anonymous"></script>
    <title>Register</title>
</head>

<style>
    .error-message {
        color: red;
        background-color: #ffe0e0;
        border: 1px solid #ff8080;
        padding: 10px;
        margin-bottom: 10px;
        border-radius: 4px;
    }
</style>

<body>
    <div>
        <?php 
        session_start();
        include "db.php";
        if (isset($_POST["f_name"])) {
        
            $f_name = $_POST["f_name"];
            $l_name = $_POST["l_name"];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $repassword = $_POST['repassword'];
            $mobile = $_POST['mobile'];
            $address1 = $_POST['address1'];
            $address2 = $_POST['address2'];
            $name = "/^[a-zA-Z ]+$/";
            $emailValidation = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9]+(\.[a-z]{2,4})$/";
            $number = "/^[0-9]+$/";

            $errors = array();

            if(!preg_match($name,$f_name)){
                $errors[] = "First name ".$f_name." is not valid.";
            }
            if(!preg_match($name,$l_name)){
                $errors[] = "Last name ".$l_name." is not valid.";
            }
            if(!preg_match($emailValidation,$email)){
                $errors[] = "Email is not valid.";
            }
            if(strlen($password) < 9 || strlen($repassword) < 9 ){
                $errors[] = "Password is too short.";
            }
            if($password != $repassword){
                $errors[] = "Passwords are not matching.";
            }
            if(!preg_match($number,$mobile)){
                $errors[] = "Mobile number is not valid.";
            }
            //existing email address in our database
            $sql = "SELECT user_id FROM user_info WHERE email = '$email' LIMIT 1" ;
            $check_query = mysqli_query($con,$sql);
            $count_email = mysqli_num_rows($check_query);
            if($count_email > 0){
                $errors[] = "Email address is already in use.";
            }

            if (!empty($errors)) {
                echo '<div class="error-message">';
                echo '<p><strong>Oops! There were some errors:</strong></p>';
                echo '<ul>';
                foreach ($errors as $error) {
                    echo '<li>' . $error . '</li>';
                }
                echo '</ul>';
                echo '</div>';
            } 
            
            if (empty($errors)){

                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
                
                $sql = "INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address1`, `address2`) 
                VALUES (NULL, '$f_name', '$l_name', '$email', '$hashedPassword', '$mobile', '$address1', '$address2')";
                $run_query = mysqli_query($con,$sql);
                $_SESSION["uid"] = mysqli_insert_id($con);
                $_SESSION["name"] = $f_name;
                $ip_add = getenv("REMOTE_ADDR");
                $sql = "UPDATE cart SET user_id = '$_SESSION[uid]' WHERE ip_add='$ip_add' AND user_id = -1";
                if(mysqli_query($con,$sql)){
                    echo "register_success";
                    echo "<script> location.href='store.php'; </script>";
                    exit;
                }
            }      
        }
        ?>
    </div>
    <form action="" method="post">
        <div class="container">
            <h1>Register</h1>
            <hr>
            
            <label for="ime"><b>First name</b></label>
            <input type="text" name="f_name" id="f_name" class="form-control" placeholder= "Enter your first name" required><br>
            
            <label for="prezime"><b>Last name</b></label>
            <input type="text" name="l_name" id="l_name" class="form-control" placeholder= "Enter your last name" required><br>
            
            <label for="email"><b>Email</b></label>
            <input type="text" name="email" id="email" class="form-control" placeholder= "Enter your email" required><br>
            
            <label for="password"><b>Password</b></label>
            <input type="password" name="password" id="password" placeholder= "Enter your password" required><br>

            <label for="password"><b>Repeat password</b></label>
            <input type="password" name="repassword" id="repassword" placeholder= "Repeat you password" required><br>

            <label for="password"><b>Phone</b></label>
            <input type="text" name="mobile" id="mobile" class="form-control" placeholder= "Enter your phone number" required><br>

            <label for="password"><b>Address</b></label>
            <input type="text" name="address1" id="address1" placeholder= "Enter your address" required><br>

            <label for="password"><b>City</b></label>
            <input type="text" name="address2" id="address2" placeholder= "Enter your city" required><br>

            <div class="clearfix">
                <button type="button" class="cancelbtn" onclick="location.href='index.php'">Cancel</button>
                <button type="submit" class="signupbtn">Register</button>
            </div>
            <button type="button" onclick="location.href='prijava.php'">Want to log in?</button><hr>
        </div>
    </form>
</body>
</html>
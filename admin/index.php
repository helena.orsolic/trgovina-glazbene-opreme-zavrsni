<!DOCTYPE html>
<html>

<head>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <style>
        #chart {
            text-align: center;
            margin-top: 2px;
        }
        select[name="selectedMonth"], input[type="submit"] {
            font-size: 16px;
            padding: 10px;
        }
    </style>
</head>

<?php
session_start();
include("../db.php");

include "sidenav.php";
include "topheader.php";
?>

      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
         <div class="panel-body">
		      <a>
            <?php  //success message
            if(isset($_POST['success'])) {
            $success = $_POST["success"];
            echo "<h1 style='color:#0C0'>Your Product was added successfully &nbsp;&nbsp;  <span class='glyphicon glyphicon-ok'></h1></span>";
            }?>
            </a>
              </div>
                <div class="col-md-14">
                <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title"> Users List</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive ps">
                  <table class="table table-hover tablesorter " id="">
                    <thead class=" text-primary">
                        <tr><th>ID</th><th>FirstName</th><th>LastName</th><th>Email</th><th>Contact</th><th>Address</th>
                    </tr></thead>
                    <tbody>
                      <?php 
                        $result=mysqli_query($con,"select * from user_info")or die ("query 1 incorrect.....");

                        while(list($user_id,$first_name,$last_name,$email,$password,$phone,$address1,$address2)=mysqli_fetch_array($result))
                        {	
                        echo "<tr><td>$user_id</td><td>$first_name</td><td>$last_name</td><td>$email</td><td>$phone</td><td>$address1</td>

                        </tr>";
                        }
                        ?>
                    </tbody>
                  </table>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-md-6">
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title"> Categories List</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive ps">
                  <table class="table table-hover tablesorter " id="">
                    <thead class=" text-primary">
                        <tr><th>ID</th><th>Categories</th><th>Count</th>
                    </tr></thead>
                    <tbody>
                      <?php 
                        $result=mysqli_query($con,"select * from categories")or die ("query 1 incorrect.....");
                        $i=1;
                        while(list($cat_id,$cat_title)=mysqli_fetch_array($result))
                        {	
                            $sql = "SELECT COUNT(*) AS count_items FROM products WHERE product_cat=$i";
                            $query = mysqli_query($con,$sql);
                            $row = mysqli_fetch_array($query);
                            $count=$row["count_items"];
                            $i++;
                        echo "<tr><td>$cat_id</td><td>$cat_title</td><td>$count</td>

                        </tr>";
                        }
                        ?>
                    </tbody>
                  </table>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Brands List</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive ps">
                  <table class="table table-hover tablesorter " id="">
                    <thead class=" text-primary">
                        <tr><th>ID</th><th>Brands</th><th>Count</th>
                    </tr></thead>
                    <tbody>
                      <?php 
                        $result=mysqli_query($con,"select * from brands")or die ("query 1 incorrect.....");
                        $i=1;
                        while(list($brand_id,$brand_title)=mysqli_fetch_array($result))
                        {	
                            
                            $sql = "SELECT COUNT(*) AS count_items FROM products WHERE product_brand=$i";
                            $query = mysqli_query($con,$sql);
                            $row = mysqli_fetch_array($query);
                            $count=$row["count_items"];
                            $i++;
                        echo "<tr><td>$brand_id</td><td>$brand_title</td><td>$count</td>

                        </tr>";
                        }
                        ?>
                    </tbody>
                  </table>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
          </div>

          <div class="col-md-12" id="chart">
            <h1>Select Month</h1>
            <form method="get" id="salesForm">
                <select name="selectedMonth">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05">May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                
                <input type="submit" value="Show Sales">
            </form>
            
            <?php
            
            $selectedMonth = isset($_GET['selectedMonth']) ? $_GET['selectedMonth'] : date('m');

            // Your SQL query
            $query = "SELECT p.product_title, COUNT(o.order_id) AS purchase_count 
                      FROM products p
                      LEFT JOIN order_products o ON p.product_id = o.product_id
                      WHERE MONTH(o.order_date) = '$selectedMonth'
                      GROUP BY p.product_id
                      ORDER BY purchase_count DESC";
                      

            $result = mysqli_query($con, $query) or die("Query error...");

            $productTitles = [];
            $purchaseCounts = [];

            while ($row = mysqli_fetch_assoc($result)) {
                $productTitles[] = $row['product_title'];
                $purchaseCounts[] = $row['purchase_count'];
            }

            $monthNames = [
              "01" => "January",
              "02" => "February",
              "03" => "March",
              "04" => "April",
              "05" => "May",
              "06" => "June",
              "07" => "July",
              "08" => "August",
              "09" => "September",
              "10" => "October",
              "11" => "November",
              "12" => "December"
            ];

            $selectedMonthName = $monthNames[$selectedMonth];

            mysqli_close($con);
            ?>

            <h1>Data shown for: <?php echo $selectedMonthName; ?></h1>

            <canvas id="purchaseCountChart"></canvas>

            <script>
                var ctx = document.getElementById('purchaseCountChart').getContext('2d');
                var purchaseCountChart = new Chart(ctx, {
                    type: 'bar',
                    
                    data: {
                        labels: <?php echo json_encode($productTitles); ?>,
                        datasets: [{
                            label: 'Purchase Count',
                            data: <?php echo json_encode($purchaseCounts); ?>,
                            backgroundColor: 'rgba(255, 165, 0, 0.2)', // Light orange
                            borderColor: 'rgba(255, 165, 0, 1)', // Darker orange
                            borderWidth: 1,
                        }]
                    },
                    options: {
        scales: {
            y: {
                beginAtZero: true,
                title: {
                    display: true,
                    text: 'Purchase Count'
                },
                ticks: {
                    callback: function(value, index, values) {
                        if (Math.floor(value) === value) {
                            return value;
                        }
                    }
                }
            },
            x: {
                title: {
                    display: true,
                    text: 'Product Title'
                }
            }
        }
    }
                });

            </script>

          </div>

        </div>
      </div>
    </div>
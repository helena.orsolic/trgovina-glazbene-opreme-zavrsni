<?php
session_start();
include("../db.php");

if(isset($_GET['action']) && $_GET['action']!="" && $_GET['action']=='delete') {

  $user_id=$_GET['user_id'];

  /*this is delet query*/
  mysqli_query($con,"delete from user_info where user_id='$user_id'")or die("query is incorrect...");
}

include "sidenav.php";
include "topheader.php";
?>

<!-- End Navbar -->
<div class="content">
  <div class="container-fluid">
    <div class="col-md-14">
      <div class="card ">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Manage User</h4>
        </div>

        <div class="card-body">
          <div class="table-responsive ps">
            <table class="table tablesorter table-hover" id="">
              <thead class=" text-primary">
                <tr>
                  <th>User ID</th>
                  <th>User Name</th>
                  <th>User Lastname</th>
                  <th>User Email</th>
                  <th>Ammount spent</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $result = mysqli_query($con, "SELECT u.user_id, u.first_name, u.last_name, u.email, SUM(o.total_amt) AS total_spent 
                                                FROM user_info u
                                                LEFT JOIN orders_info o ON u.user_id = o.user_id
                                                GROUP BY u.user_id") or die ("query 2 incorrect.......");


                  while(list($user_id, $user_fname, $user_lname, $user_email, $total_spent) = mysqli_fetch_array($result)) {
                    echo "<tr>
                            <td>$user_id</td><td>$user_fname</td><td>$user_lname</td><td>$user_email</td>";

                    if ($total_spent === null) {
                        echo "<td>$ 0</td>";
                    } else {
                        echo "<td>$ $total_spent</td>";
                    }

                    echo "<td>
                            <a href='edituser.php?user_id=$user_id' type='button' rel='tooltip' title='' class='btn btn-info btn-link btn-sm' data-original-title='Edit User'>
                              <i class='material-icons'>edit</i>
                              <div class='ripple-container'></div>
                            </a>
                            <a href='manageuser.php?user_id=$user_id&action=delete' type='button' rel='tooltip' title='' class='btn btn-danger btn-link btn-sm' data-original-title='Delete User'>
                              <i class='material-icons'>close</i>
                              <div class='ripple-container'></div>
                            </a>
                          </td>
                          </tr>";
                  }


                  mysqli_close($con);
                ?>
                <th><a href="adduser.php" class="btn btn-success">Add New</a></th>
              </tbody>
            </table>

            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
              <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            
            <div class="ps__rail-y" style="top: 0px; right: 0px;">
              <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

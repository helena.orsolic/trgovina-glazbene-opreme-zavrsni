 <?php
  session_start();
  include("../db.php");
  include "sidenav.php";
  include "topheader.php";
  if(isset($_POST['btn_save'])) {

    $first_name=$_POST['first_name'];
    $last_name=$_POST['last_name'];
    $email=$_POST['email'];
    $user_password=$_POST['password'];
    $mobile=$_POST['phone'];
    $address1=$_POST['city'];
    $address2=$_POST['country'];

    $hashedPassword = password_hash($user_password, PASSWORD_DEFAULT);

    mysqli_query($con,"insert into user_info(first_name, last_name,email,password,mobile,address1,address2) 
      values ('$first_name','$last_name','$email','$user_password','$mobile','$address1','$address2')") 
      or die ("Query 1 is inncorrect........");

    header("location: manageuser.php"); 
    mysqli_close($con);
}
?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Users</h4>
                  <p class="card-category">Complete User profile</p>
                </div>
                <div class="card-body">
                  <form action="" method="post" name="form" enctype="multipart/form-data">
                    <div class="row">
                      
                      <div class="col-md-3">
                        <label class="bmd-label-floating">First Name</label>
                        <div class="form-group bmd-form-group">
                          <input type="text" id="first_name" name="first_name" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <label class="bmd-label-floating">Last Name</label>
                        <div class="form-group bmd-form-group">
                          <input type="text" name="last_name" id="last_name"  class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label class="bmd-label-floating">Email</label>
                        <div class="form-group bmd-form-group">
                          <input type="email" name="email" id="email" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label class="bmd-label-floating">Password</label>
                        <div class="form-group bmd-form-group">
                          <input type="password" id="password" name="password" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label class="bmd-label-floating">Phone number</label>
                        <div class="form-group bmd-form-group">
                          <input type="text" id="phone" name="phone" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <label class="bmd-label-floating">Address</label>
                        <div class="form-group bmd-form-group">
                          <input type="text" name="country" id="country" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary pull-right">Add New User</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
        </div>
      </div>
